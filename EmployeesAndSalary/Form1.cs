﻿﻿using System;
using System.Data;
using System.Drawing;
using System.Windows.Forms;

namespace EmployeesAndSalary
{
    #region ЗАДАНИЕ
    //Написать приложение на C#, используя ADO.NET:
    //1. Создать базу данных с 2 таблицами:
    //  1) Список сотрудников.Поля: Id, Name (имя сотрудника), Active (булевая переменная (тип bit), указывающая на то, работает ли данный сотрудник на данный момент)
    //  2) Таблица с зарплатами сотрудников.Поля: Id (сотрудника), Salary (выплата), Datetime (время выплаты). Примечание: могут быть сотрудники, которым не выплатили ни разу, а также выплаты могут осуществляться несколько раз в месяц.
    //2. Написать графическую часть на Winforms или WPF.
    //Присутствуют элементы:
    //  1) Комбобокс с выпадающим списком для выбора параметров:
    //  - "Средняя за месяц"
    //  - "Максимальная за месяц"
    //  2) Поле для поиска по имени сотрудников.
    //  3) Чекбокс "показать уволенных" (к общему результату добавляются уволенные сотрудники)
    //  4) Кнопка "показать". При нажатии на кнопку из базы данных выводятся имена сотрудников и их максимальная/средняя зарплату 
    //  в зависимости от выбранного параметра, отсортировать по убыванию значения зарплаты.
    //  У тех сотрудников, у которых еще не было выплат в этом месяце, вывести 0.
    //  Выделять каким-нибудь цветом в списке тех, у кого текущий показатель зарплаты меньше 20000.
    //  5) Какой-нибудь элемент с отображением списка, куда вводится список сотрудников.
    #endregion

    public partial class Form1 : Form
    {
        private WorkDB workDB;
        public Form1()
        {
            InitializeComponent();
            cbSalaryParameter.Items.AddRange(new string[] { "Средняя за месяц", "Максимальная за месяц" });
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            workDB = new WorkDB();
            workDB.CreateDB();
        }

        private void btnShow_Click(object sender, EventArgs e)
        {
            if(cbSalaryParameter.SelectedIndex<0)
            {
                MessageBox.Show("Укажите искать среднюю или максимальную ЗП, это обязательный параметр");
                return;
            }

            DataTable dateTable = CreateData();
            if (dateTable == null)
                return;
                
            dgvResult.DataSource = dateTable;
            dgvResult.Update();
            dgvResult.Columns[0].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            dgvResult.Columns[1].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;

            ColorRow();
        }
        #region Метод заливки строки цветом
        /// <summary>
        /// Метод заливки строки цветом
        /// </summary>
        void ColorRow()
        {
            decimal minValSalary = 20000;
            for (int i = dgvResult.RowCount - 1; i >= 0; i--)
            {
                if (Convert.ToDecimal(dgvResult[1, i].Value) < minValSalary)
                    dgvResult.Rows[i].DefaultCellStyle.BackColor = Color.Aqua;
                else
                    break;
            }
        }
        #endregion

        #region Метод формирующий Select и добывающий данные в DataTable
        /// <summary>
        /// Метод формирующий Select и добывающий данные в DataTable
        /// </summary>
        /// <returns></returns>
        private DataTable CreateData()
        {
            string funct1 = null;
            string funct2 = null;
            string nameMask = tbSearchName.Text;
            string active = null;
            if (chbShowDismissed.Checked)
                active = ",'false'";

            if (cbSalaryParameter.SelectedItem.ToString() == "Средняя за месяц")
            {
                funct1 = "AVG";
                funct2 = "AVG";
            }
            if (cbSalaryParameter.SelectedItem.ToString() == "Максимальная за месяц")
            {
                funct1 = "SUM";
                funct2 = "MAX";
            }

            string select = string.Format("SELECT t1.Name AS ФИО ," +
                            "{0}(t1.Salary) AS Доход " +
                            "FROM (SELECT e.Name, "+ 
                                         "coalesce({1}(s.Salary), 0) AS Salary "+
                                    "FROM Employees e "+
                                    "LEFT JOIN Salaries s "+
                                    "ON s.Id = e.Id "+
                                        "WHERE e.Active in ('true'{2}) "+
                                        "AND e.Name Like '%{3}%' "+
                                            "GROUP BY DATEPART(year,s.Datetime), "+ 
                                                     "DATEPART(month,s.Datetime), "+ 
                                                     "e.Name) AS t1 "+
                                "GROUP BY t1.Name "+
                                    "ORDER BY Доход DESC", funct2, funct1, active, nameMask);
                                    
            return workDB.SelectDB(select);
        }
        #endregion

        private void btnAddEmployees_Click(object sender, EventArgs e)
        {
            AddEmployees addEmployees = new AddEmployees();
            addEmployees.Show();
        }

        private void btnAddSalaries_Click(object sender, EventArgs e)
        {
            AddSalaries addSalaries = new AddSalaries();
            addSalaries.Show();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            workDB.DBInsertString("DELETE FROM Salaries");
            workDB.DBInsertString("DELETE FROM Employees");
            MessageBox.Show("Данные удалены");
        }
    }
}
