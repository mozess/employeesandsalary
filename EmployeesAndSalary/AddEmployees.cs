﻿﻿using System;
using System.Windows.Forms;

namespace EmployeesAndSalary
{
    public partial class AddEmployees : Form
    {
        private WorkDB workDB;
        public AddEmployees()
        {
            InitializeComponent();
            workDB = new WorkDB();
        }

        private void AddEmployee_Click(object sender, EventArgs e)
        {
            if (tbName.Text.Length == 0)
            {
                MessageBox.Show("Укажите ФИО");
                return;
            }
            if (workDB.SelectDB(string.Format("SELECT Name FROM Employees WHERE Name='{0}'", tbName.Text)).Rows.Count > 0)
            {
                MessageBox.Show("Такой сотрудник уже существует");
                return;
            }
            workDB.DBInsertString(string.Format("INSERT INTO Employees (Name, Active) VALUES ('{0}','{1}')",tbName.Text,chbAction.Checked));
            MessageBox.Show("Добавил");
        }
    }
}
