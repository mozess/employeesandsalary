﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlServerCe;
using System.IO;
using System.Windows.Forms;


namespace EmployeesAndSalary
{
    class WorkDB
    {
        private static string fileName = "EmployeesDB.sdf";
        private static string password = "mypassword";
        private static string connectionString = string.Format($"DataSource=\"{fileName}\"; Password='{password}'");

        # region Метод создания и заполнения БД
        /// <summary>
        /// Метод создания и заполнения БД
        /// </summary>
        public void CreateDB()
        {
            //1. Создать базу данных с 2 таблицами:
            if (File.Exists(fileName))
                return;

            SqlCeEngine sqlCeEngine = new SqlCeEngine(connectionString);
            sqlCeEngine.CreateDatabase();

            //1) Список сотрудников.Поля: Id, Name(имя сотрудника), Active(булевая переменная(тип bit), указывающая на то, работает ли данный сотрудник на данный момент)
            string createEmployees = "create table Employees(Id int NOT NULL IDENTITY(1,1) PRIMARY KEY, Name NVARCHAR(100) NOT NULL, Active bit NOT NULL DEFAULT 1)";
            SqlCmd(createEmployees);


            #region Заполнение списка данными для Таблицы Employees
            List<string> insertEmployees = new List<string>();
            insertEmployees.Add("INSERT INTO Employees (Name) VALUES ('Пирогова Светлана Васильевна')");
            insertEmployees.Add("INSERT INTO Employees (Name) VALUES ('Светиков Игор Вячеславович')");
            insertEmployees.Add("INSERT INTO Employees (Name) VALUES ('Крошев Василий Геннадьевич')");
            insertEmployees.Add("INSERT INTO Employees (Name) VALUES ('Троицкий Игорь Васильевич')");
            insertEmployees.Add("INSERT INTO Employees (Name) VALUES ('Жаворонков Илья Александрович')");
            insertEmployees.Add("INSERT INTO Employees (Name) VALUES ('Пиманова Екатерина Олеговна')");
            insertEmployees.Add("INSERT INTO Employees (Name) VALUES ('Динаров Лев Петрович')");
            insertEmployees.Add("INSERT INTO Employees (Name) VALUES ('Корнеева Анастасия Игоревна')");
            insertEmployees.Add("INSERT INTO Employees (Name) VALUES ('Ларченко Денис Денисович')");
            insertEmployees.Add("INSERT INTO Employees (Name) VALUES ('Кроликова Раиса Ивановна')");
            insertEmployees.Add("INSERT INTO Employees (Name) VALUES ('Терпилко Леонид Алексеевич')");
            insertEmployees.Add("INSERT INTO Employees (Name) VALUES ('Рукомойникова Алина Альбертовна')");
            insertEmployees.Add("INSERT INTO Employees (Name) VALUES ('Жирков Павел Васильевич')");
            insertEmployees.Add("INSERT INTO Employees (Name) VALUES ('Нареченская Любовь Александровна')");
            insertEmployees.Add("INSERT INTO Employees (Name) VALUES ('Ферзев Алексей Геннадьевич')");
            insertEmployees.Add("INSERT INTO Employees (Name) VALUES ('Розова Любовь Алексеевна')");
            insertEmployees.Add("INSERT INTO Employees (Name) VALUES ('Соков Кирилл Александрович')");
            insertEmployees.Add("INSERT INTO Employees (Name) VALUES ('Комьева Анастасия Львовна')");
            insertEmployees.Add("INSERT INTO Employees (Name) VALUES ('Камешкина Елена Алексеевна')");
            insertEmployees.Add("INSERT INTO Employees (Name) VALUES ('Телефонов Геннадий Алексеевич')");
            insertEmployees.Add("INSERT INTO Employees (Name,Active) VALUES ('Ергина Людмила Львовна',0)");
            insertEmployees.Add("INSERT INTO Employees (Name,Active) VALUES ('Деточкин Олег Олегович',0)");
            insertEmployees.Add("INSERT INTO Employees (Name,Active) VALUES ('Деточкина Екатерина Романовна',0)");
            insertEmployees.Add("INSERT INTO Employees (Name,Active) VALUES ('Стулов Алексей Евгеньевич',0)");
            insertEmployees.Add("INSERT INTO Employees (Name,Active) VALUES ('Оконцева Галина Альбертовна',0)");
            insertEmployees.Add("INSERT INTO Employees (Name,Active) VALUES ('Романова Дина Владимировна',0)");
            insertEmployees.Add("INSERT INTO Employees (Name,Active) VALUES ('Грозов Павел Федорович',0)");
            insertEmployees.Add("INSERT INTO Employees (Name,Active) VALUES ('Данилкина Любовь Николаевна',0)");
            insertEmployees.Add("INSERT INTO Employees (Name,Active) VALUES ('Петрушев Константин Константинович',0)");
            insertEmployees.Add("INSERT INTO Employees (Name,Active) VALUES ('Горюшева Елена Николаевна',0)");
            DBInsertList(insertEmployees);
            #endregion

            //2) Таблица с зарплатами сотрудников.Поля: Id(сотрудника), Salary(выплата), Datetime(время выплаты).Примечание: могут быть сотрудники, которым не выплатили ни разу, а также выплаты могут осуществляться несколько раз в месяц.
            string createSalaries = "create table Salaries(Id int NOT NULL, Salary MONEY NOT NULL, Datetime DATETIME NOT NULL)";
            string alterSalaries = "ALTER TABLE Salaries ADD CONSTRAINT FK_ID FOREIGN KEY(Id) REFERENCES Employees(Id)"; 
            SqlCmd(createSalaries);
            SqlCmd(alterSalaries);

            #region Заполнение списка данными для Таблицы Salaries
            List<string> insertSalaries = new List<string>();
            insertSalaries.Add("INSERT INTO Salaries (Id,Salary,DateTime) VALUES ('1','15000',CONVERT(datetime,'2019-01-30 13:00:00',20))");
            insertSalaries.Add("INSERT INTO Salaries (Id,Salary,DateTime) VALUES ('2','30000',CONVERT(datetime,'2019-01-30 13:00:00',20))");
            insertSalaries.Add("INSERT INTO Salaries (Id,Salary,DateTime) VALUES ('6','50000',CONVERT(datetime,'2019-01-30 13:00:00',20))");
            insertSalaries.Add("INSERT INTO Salaries (Id,Salary,DateTime) VALUES ('7','1000',CONVERT(datetime,'2019-01-30 13:00:00',20))");
            insertSalaries.Add("INSERT INTO Salaries (Id,Salary,DateTime) VALUES ('8','12000',CONVERT(datetime,'2019-01-30 13:00:00',20))");
            insertSalaries.Add("INSERT INTO Salaries (Id,Salary,DateTime) VALUES ('9','25000',CONVERT(datetime,'2019-01-30 13:00:00',20))");
            insertSalaries.Add("INSERT INTO Salaries (Id,Salary,DateTime) VALUES ('10','45000',CONVERT(datetime,'2019-01-30 13:00:00',20))");
            insertSalaries.Add("INSERT INTO Salaries (Id,Salary,DateTime) VALUES ('11','10000',CONVERT(datetime,'2019-01-30 13:00:00',20))");
            insertSalaries.Add("INSERT INTO Salaries (Id,Salary,DateTime) VALUES ('12','15000',CONVERT(datetime,'2019-01-30 13:00:00',20))");
            insertSalaries.Add("INSERT INTO Salaries (Id,Salary,DateTime) VALUES ('13','30000',CONVERT(datetime,'2019-01-30 13:00:00',20))");
            insertSalaries.Add("INSERT INTO Salaries (Id,Salary,DateTime) VALUES ('14','12000',CONVERT(datetime,'2019-01-30 13:00:00',20))");
            insertSalaries.Add("INSERT INTO Salaries (Id,Salary,DateTime) VALUES ('15','8000',CONVERT(datetime,'2019-01-30 13:00:00',20))");
            insertSalaries.Add("INSERT INTO Salaries (Id,Salary,DateTime) VALUES ('16','5000',CONVERT(datetime,'2019-01-30 13:00:00',20))");
            insertSalaries.Add("INSERT INTO Salaries (Id,Salary,DateTime) VALUES ('17','50000',CONVERT(datetime,'2019-01-30 13:00:00',20))");
            insertSalaries.Add("INSERT INTO Salaries (Id,Salary,DateTime) VALUES ('18','1000',CONVERT(datetime,'2019-01-30 13:00:00',20))");
            insertSalaries.Add("INSERT INTO Salaries (Id,Salary,DateTime) VALUES ('19','12000',CONVERT(datetime,'2019-01-30 13:00:00',20))");
            insertSalaries.Add("INSERT INTO Salaries (Id,Salary,DateTime) VALUES ('20','25000',CONVERT(datetime,'2019-01-30 13:00:00',20))");
            insertSalaries.Add("INSERT INTO Salaries (Id,Salary,DateTime) VALUES ('21','45000',CONVERT(datetime,'2019-01-30 13:00:00',20))");
            insertSalaries.Add("INSERT INTO Salaries (Id,Salary,DateTime) VALUES ('22','10000',CONVERT(datetime,'2019-01-30 13:00:00',20))");
            insertSalaries.Add("INSERT INTO Salaries (Id,Salary,DateTime) VALUES ('23','15000',CONVERT(datetime,'2019-01-30 13:00:00',20))");
            insertSalaries.Add("INSERT INTO Salaries (Id,Salary,DateTime) VALUES ('24','30000',CONVERT(datetime,'2019-01-30 13:00:00',20))");
            insertSalaries.Add("INSERT INTO Salaries (Id,Salary,DateTime) VALUES ('25','12000',CONVERT(datetime,'2019-01-30 13:00:00',20))");
            insertSalaries.Add("INSERT INTO Salaries (Id,Salary,DateTime) VALUES ('26','8000',CONVERT(datetime,'2019-01-30 13:00:00',20))");
            insertSalaries.Add("INSERT INTO Salaries (Id,Salary,DateTime) VALUES ('27','5000',CONVERT(datetime,'2019-01-30 13:00:00',20))");
            insertSalaries.Add("INSERT INTO Salaries (Id,Salary,DateTime) VALUES ('28','50000',CONVERT(datetime,'2019-01-30 13:00:00',20))");
            insertSalaries.Add("INSERT INTO Salaries (Id,Salary,DateTime) VALUES ('29','1000',CONVERT(datetime,'2019-01-30 13:00:00',20))");
            insertSalaries.Add("INSERT INTO Salaries (Id,Salary,DateTime) VALUES ('10','45000',CONVERT(datetime,'2019-01-30 13:00:00',20))");
            insertSalaries.Add("INSERT INTO Salaries (Id,Salary,DateTime) VALUES ('11','100',CONVERT(datetime,'2019-01-30 13:00:00',20))");
            insertSalaries.Add("INSERT INTO Salaries (Id,Salary,DateTime) VALUES ('12','8000',CONVERT(datetime,'2019-01-30 13:00:00',20))");
            insertSalaries.Add("INSERT INTO Salaries (Id,Salary,DateTime) VALUES ('13','2000',CONVERT(datetime,'2019-01-30 13:00:00',20))");
            insertSalaries.Add("INSERT INTO Salaries (Id,Salary,DateTime) VALUES ('14','5000',CONVERT(datetime,'2019-01-30 13:00:00',20))");
            insertSalaries.Add("INSERT INTO Salaries (Id,Salary,DateTime) VALUES ('11','13000',CONVERT(datetime,'2019-01-30 13:00:00',20))");
            insertSalaries.Add("INSERT INTO Salaries (Id,Salary,DateTime) VALUES ('12','800',CONVERT(datetime,'2019-01-30 13:00:00',20))");
            DBInsertList(insertSalaries);
            #endregion
        }
        #endregion

        #region Метод добавления данных из списка в БД
        /// <summary>
        /// Метод добавления данных из списка в БД (нужно для заполнения БД первоначальными данными)
        /// </summary>
        /// <param name="ListInsert"></param>
        private void DBInsertList(List<string> ListInsert)
        {
            var myConnection = new SqlCeConnection(connectionString);
            try
            {
                myConnection.Open();
            }
            catch (Exception ex)
            {
                MessageBox.Show("## !!!ОПЕРАЦИЯ ПРЕРВАНА, ОШИБКА ПРИ ПОДКЛЮЧЕНИЕ К БД: " + "\n" + ex.Message);
                return;
            }

            var transaction = myConnection.BeginTransaction();

            var cmd = myConnection.CreateCommand();
            cmd.Transaction = transaction;

            try
            {
                foreach (string query in ListInsert)
                {
                    cmd.CommandText = query;
                    cmd.ExecuteNonQuery();
                }
                transaction.Commit();
                myConnection.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("## !!!ОПЕРАЦИЯ ПРЕРВАНА, ОШИБКА ПРИ ДОБАВЛЕНИИ ДАННЫХ БД: " + "\n" + ex.Message);
                transaction.Rollback();
                myConnection.Close();
                return;
            }
        }
        #endregion

        #region Метод добавления одной строки в БД
        /// <summary>
        /// Метод добавления одной строки в БД (нужно для заполнения БД данными)
        /// </summary>
        /// <param name="insert">Строка запроса insert</param>
        public void DBInsertString(string insert)
        {
            var myConnection = new SqlCeConnection(connectionString);
            try
            {
                myConnection.Open();
            }
            catch (Exception ex)
            {
                MessageBox.Show("## !!!ОПЕРАЦИЯ ПРЕРВАНА, ОШИБКА ПРИ ПОДКЛЮЧЕНИЕ К БД: " + "\n" + ex.Message);
                return;
            }
            var cmd = myConnection.CreateCommand();
            try
            {
                cmd.CommandText = insert;
                cmd.ExecuteNonQuery();
                myConnection.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("## !!!ОПЕРАЦИЯ ПРЕРВАНА, ОШИБКА ПРИ ДОБАВЛЕНИИ ДАННЫХ БД: " + "\n" + ex.Message);
                myConnection.Close();
                return;
            }
        }
        #endregion

        #region Метод для команды создания БД
        /// <summary>
        /// Метод для команды создания БД
        /// </summary>
        /// <param name="query">Запрос</param>
        private void SqlCmd(string query)
        {
            SqlCeConnection myConnection = new SqlCeConnection(connectionString);
            SqlCeCommand cmd = new SqlCeCommand(query, myConnection);
            try
            {
                myConnection.Open();
            }
            catch (Exception ex)
            {
                MessageBox.Show("## !!!ОПЕРАЦИЯ ПРЕРВАНА, ОШИБКА ПРИ ПОДКЛЮЧЕНИЕ К БД: " + "\n" + ex.Message);
                return;
            }
            try
            {
                cmd.ExecuteNonQuery();
                myConnection.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("## !!!ОПЕРАЦИЯ ПРЕРВАНА, ОШИБКА ПРИ РАБОТЕ С БД: " + "\n" + ex.Message);
                myConnection.Close();
                return;
            }
        }
        #endregion

        #region Метод получения данных из БД
        /// <summary>
        /// Метод получения данных из БД
        /// </summary>
        /// <param name="select">строка Select</param>
        /// <returns></returns>
        public DataTable SelectDB(string select)
        {
            var sqlCeConnection = new SqlCeConnection(connectionString);
            var dataAdapter = new SqlCeDataAdapter(select, sqlCeConnection);
            var commandBuilder = new SqlCeCommandBuilder(dataAdapter);
            var dataSet = new DataSet();
            try
            {
                dataAdapter.Fill(dataSet);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Возникла проблема при подключение к бд: " + ex.Message);
                return null;
            }
            return dataSet.Tables[0];
        }
        #endregion
    }
}
