﻿namespace EmployeesAndSalary
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.cbSalaryParameter = new System.Windows.Forms.ComboBox();
            this.chbShowDismissed = new System.Windows.Forms.CheckBox();
            this.tbSearchName = new System.Windows.Forms.TextBox();
            this.btnShow = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.dgvResult = new System.Windows.Forms.DataGridView();
            this.btnAddEmployees = new System.Windows.Forms.Button();
            this.btnAddSalaries = new System.Windows.Forms.Button();
            this.btnDelete = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgvResult)).BeginInit();
            this.SuspendLayout();
            // 
            // cbSalaryParameter
            // 
            this.cbSalaryParameter.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cbSalaryParameter.FormattingEnabled = true;
            this.cbSalaryParameter.Location = new System.Drawing.Point(124, 12);
            this.cbSalaryParameter.Name = "cbSalaryParameter";
            this.cbSalaryParameter.Size = new System.Drawing.Size(329, 21);
            this.cbSalaryParameter.TabIndex = 0;
            // 
            // chbShowDismissed
            // 
            this.chbShowDismissed.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.chbShowDismissed.AutoSize = true;
            this.chbShowDismissed.Location = new System.Drawing.Point(307, 66);
            this.chbShowDismissed.Name = "chbShowDismissed";
            this.chbShowDismissed.Size = new System.Drawing.Size(146, 17);
            this.chbShowDismissed.TabIndex = 1;
            this.chbShowDismissed.Text = "Показывать уволенных";
            this.chbShowDismissed.UseVisualStyleBackColor = true;
            // 
            // tbSearchName
            // 
            this.tbSearchName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbSearchName.Location = new System.Drawing.Point(124, 39);
            this.tbSearchName.Name = "tbSearchName";
            this.tbSearchName.Size = new System.Drawing.Size(329, 20);
            this.tbSearchName.TabIndex = 2;
            // 
            // btnShow
            // 
            this.btnShow.Location = new System.Drawing.Point(15, 129);
            this.btnShow.Name = "btnShow";
            this.btnShow.Size = new System.Drawing.Size(106, 42);
            this.btnShow.TabIndex = 3;
            this.btnShow.Text = "Показать";
            this.btnShow.UseVisualStyleBackColor = true;
            this.btnShow.Click += new System.EventHandler(this.btnShow_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 42);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(94, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Искать по имени";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(9, 15);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(109, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = "Параметр выгрузки";
            // 
            // dgvResult
            // 
            this.dgvResult.AllowUserToAddRows = false;
            this.dgvResult.AllowUserToDeleteRows = false;
            this.dgvResult.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvResult.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvResult.Location = new System.Drawing.Point(15, 177);
            this.dgvResult.Name = "dgvResult";
            this.dgvResult.ReadOnly = true;
            this.dgvResult.Size = new System.Drawing.Size(438, 225);
            this.dgvResult.TabIndex = 7;
            // 
            // btnAddEmployees
            // 
            this.btnAddEmployees.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAddEmployees.Location = new System.Drawing.Point(307, 89);
            this.btnAddEmployees.Name = "btnAddEmployees";
            this.btnAddEmployees.Size = new System.Drawing.Size(146, 23);
            this.btnAddEmployees.TabIndex = 8;
            this.btnAddEmployees.Text = "Добавить сотрудников";
            this.btnAddEmployees.UseVisualStyleBackColor = true;
            this.btnAddEmployees.Click += new System.EventHandler(this.btnAddEmployees_Click);
            // 
            // btnAddSalaries
            // 
            this.btnAddSalaries.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAddSalaries.Location = new System.Drawing.Point(307, 119);
            this.btnAddSalaries.Name = "btnAddSalaries";
            this.btnAddSalaries.Size = new System.Drawing.Size(146, 22);
            this.btnAddSalaries.TabIndex = 9;
            this.btnAddSalaries.Text = "Добавить деньги";
            this.btnAddSalaries.UseVisualStyleBackColor = true;
            this.btnAddSalaries.Click += new System.EventHandler(this.btnAddSalaries_Click);
            // 
            // btnDelete
            // 
            this.btnDelete.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDelete.Location = new System.Drawing.Point(307, 148);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(146, 23);
            this.btnDelete.TabIndex = 10;
            this.btnDelete.Text = "Очистить таблицы";
            this.btnDelete.UseVisualStyleBackColor = true;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(466, 415);
            this.Controls.Add(this.btnDelete);
            this.Controls.Add(this.btnAddSalaries);
            this.Controls.Add(this.btnAddEmployees);
            this.Controls.Add(this.dgvResult);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnShow);
            this.Controls.Add(this.tbSearchName);
            this.Controls.Add(this.chbShowDismissed);
            this.Controls.Add(this.cbSalaryParameter);
            this.Name = "Form1";
            this.Text = "Employees And Salary";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvResult)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox cbSalaryParameter;
        private System.Windows.Forms.CheckBox chbShowDismissed;
        private System.Windows.Forms.TextBox tbSearchName;
        private System.Windows.Forms.Button btnShow;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DataGridView dgvResult;
        private System.Windows.Forms.Button btnAddEmployees;
        private System.Windows.Forms.Button btnAddSalaries;
        private System.Windows.Forms.Button btnDelete;
    }
}

