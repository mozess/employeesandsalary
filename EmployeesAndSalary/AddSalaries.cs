﻿﻿using System;
using System.Windows.Forms;

namespace EmployeesAndSalary
{
    public partial class AddSalaries : Form
    {
        private WorkDB workDB;
        public AddSalaries()
        {
            InitializeComponent();
            workDB=new WorkDB();
        }

        private void AddSalaries_Load(object sender, EventArgs e)
        {
            var dt=workDB.SelectDB("SELECT Name FROM Employees WHERE Active = 'true' ORDER BY Name");
            for (int i = 0; i < dt.Rows.Count; i++)
                cbEmployees.Items.Add(dt.Rows[i][0]);
        }

        private void btnAddSalary_Click(object sender, EventArgs e)
        {
            string idEmployee;
            decimal sum;

            if (!decimal.TryParse(tbSumma.Text, out sum))
            {
                MessageBox.Show("Введите корректную сумму");
                return;
            }
            if (cbEmployees.SelectedIndex < 0 || cbEmployees.SelectedItem.ToString()=="")
            {
                MessageBox.Show("Выберите сотрудника");
                return;
            }
            dtpDateTime.Value = DateTime.Now;
            idEmployee=workDB.SelectDB(string.Format("SELECT Id FROM Employees WHERE Name = '{0}'", cbEmployees.SelectedItem.ToString())).Rows[0][0].ToString();
            workDB.DBInsertString(string.Format("INSERT INTO Salaries (Id,Salary,Datetime) VALUES ('{0}','{1}','{2}')",idEmployee,sum,dtpDateTime.Value.Date));
            MessageBox.Show("Добавил");
        }
    }
}
